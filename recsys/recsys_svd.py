from configparser import ConfigParser
from component.sqlite import SQLite
import pandas as pd
from logs.logs import Logs

l = Logs()

class RecsysSVD:
    
    
    def __init__(self):
        self.config = ConfigParser()
        self.config.read('config/config.ini')
        self.s = SQLite(self.config['sqlite']['location'])
        
        self.train_data_created()
        self.fit()
        
    
    def train_data_created(self):    
        result = self.s.get_all_ratings_data()
        self.df = pd.DataFrame(result, columns= ['user_id', 'movie_id', 'score'])
    
    
    def fit(self):
        self.df_pivot = pd.pivot_table(self.df, values = 'score', index = 'user_id', columns = 'movie_id')
        self.df_pivot = self.df_pivot.fillna(0)
        
    
    def predict_one(self, user_id, movie_id):
        try:
            return self.df_pivot[movie_id][user_id]
        except:
            return float(self.config['recsys']['mean_score'])
    
    
    def predict(self, user_id_list, movie_id_list):
        result = []
        for user, movie in zip(user_id_list, movie_id_list):
            result.append(self.predict_one(user, movie))
            
        