from component.sqlite import SQLite
from logs.logs import Logs
from configparser import ConfigParser
from recsys.recsys_svd import RecsysSVD


class Model:
    
    
    def __init__(self):
        self.config = ConfigParser()
        self.config.read('config/config.ini')

        self.l = Logs()
        self.r = RecsysSVD()
        self.s = SQLite(self.config['sqlite']['location'])
    
    
    def add_new_film(self, name, year, genres_list):
        title = '{} ({})'.format(name, year)
        genres = "|".join(genres_list) 
        self.s.new_film(title, genres)
        
        
    def get_score_user_movie(self, user, movie):
        return self.r.predict_one(user,movie)
    
    
    def update_user_score(self, user_id, movie_id, rating):
        self.s.update_user_film_rating(user_id, movie_id, rating)
        
    
    def add_user_score(self, user_id, movie_id, rating):
        self.s.new_user_film_rating(user_id, movie_id, rating)